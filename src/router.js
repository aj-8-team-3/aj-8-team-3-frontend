import {createRouter, createWebHistory} from "vue-router";
import Home from "./pages/Home.vue";
import NotFound from "@/pages/NotFound.vue";
import VerificationMessage from "@/pages/VerificationMessage.vue";
import SubscriptionPage from "@/pages/SubscriptionPage.vue";
import TestConstruction from "@/pages/TestConstructor.vue";
import testSelectPage from "@/pages/testSelectPage.vue";
import CompanyCabinet from "@/pages/CompanyCabinet.vue";
import PersonalCabinet from "./pages/PersonalCabinet.vue";
import SuperAdminCabinet from "@/pages/SuperAdminCabinet.vue";
import AdminCabinet from "@/pages/AdminCabinet.vue";
import TakeTestPage from "@/pages/TakingTestPage.vue";
import TakingTestPage from "@/pages/TakingTestPage.vue";
import AccessDeniedPage from "@/pages/AccessDeniedPage.vue";
import SampleTestPage from "@/pages/SampleTestPage.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/verification-msg/:token?",
        name: "VerificationMessage",
        component: VerificationMessage,
        props: (route) => ({ token: route.params.token })
    },
    {
        path: "/:pathMatch(.*)*",
        name: "NotFound",
        component: NotFound,
    },
    {
        path: "/subscription",
        name: "GetSub",
        component: SubscriptionPage,
    },
    {
        path: "/constructor",
        name: "TestConstructor",
        component: TestConstruction,
    },
    {
        path: "/tests",
        name: "Select test",
        component: testSelectPage,
    },
    {
        path: "/company-cabinet",
        name: "CompanyCabinet",
        component: CompanyCabinet,
    },
    {
        path: "/personal-cabinet",
        name: "Personal_cabinet",
        component: PersonalCabinet,
    },
    {
        path: "/admin-cabinet",
        name: "AdminCabinet",
        component: AdminCabinet,
    },
    {
        path: "/super-admin-cabinet",
        name: "SuperAdminCabinet",
        component: SuperAdminCabinet,
    },
    {
        path: '/taking-test',
        name: 'TakingTest',
        component: TakingTestPage,
    },
    {
        path: '/access-denied',
        name: 'AccessDenied',
        component: AccessDeniedPage
    },
    {
        path: '/sample-test',
        name: 'SampleTest',
        component: SampleTestPage
    }
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
});

export default router;
