import { createStore } from "vuex";

export default createStore({
  state: {
    apiUrl: "http://localhost:8000",
    testVariantToRender: {},
    sampleTest: {},
    restTime: "00:00:00",
  },
  getters: {
    apiUrl: (state) => state.apiUrl,
    restTime: (state) => state.restTime,
  },
  mutations: {
    updateRestTime(state, time) {
      state.restTime = time;
    },
  },
  actions: {
    setRestTime({ commit }, time) {
      commit("updateRestTime", time);
    },
  },
});
