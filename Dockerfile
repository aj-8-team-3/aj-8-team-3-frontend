# Этап сборки
FROM node:20-alpine as build

# Установка рабочей директории
WORKDIR /app

# Копирование файлов проекта
COPY ./package*.json .

# Установка зависимостей
RUN npm ci

COPY . .

# Сборка приложения
RUN npm run build

# Этап выполнения
FROM nginx:stable-alpine as production

# Копирование собранных файлов в Nginx
COPY --from=build /app/dist /usr/share/nginx/html

# Копируем конфигурацию приложения для nginx
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf

# Открытие порта 80 для доступа к приложению
EXPOSE 80

# Запуск Nginx и обслуживание статических файлов
CMD ["nginx", "-g", "daemon off;"]
